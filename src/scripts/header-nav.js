'use strict'

let navButton = $('#nav-button');
let navMenu = $('#nav-menu');
let docElem = document.documentElement;

navButton.addEventListener('click', function() {
    this.classList.toggle('-active');

    docElem.classList.toggle('noscroll');

    navMenu.classList.toggle('-floating');
});

docElem.addEventListener('click', function(event) {
    if (event.target === docElem) {
        navButton.classList.remove('-active');

        docElem.classList.remove('noscroll');

        navMenu.classList.remove('-floating');
    }
});
