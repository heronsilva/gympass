'use strict'

let faqItemTitles = document.querySelectorAll('.faq-item-title');

faqItemTitles.forEach(title => title.addEventListener('click', () => title.nextElementSibling.classList.toggle('-visible')));
