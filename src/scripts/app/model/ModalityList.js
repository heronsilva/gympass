class ModalityList {
    constructor() {
        this._modalities = [];
    }

    add(modality) {
        this._modalities.push(modality);
    }

    get modalities() {
        return [].concat(this._modalities);
    }
}
