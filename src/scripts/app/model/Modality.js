class Modality {
    constructor(title, type, description) {
        this._title = title;
        this._type = type;
        this._description = description;
    }

    get title() {
        return this._title;
    }

    get type() {
        return this._type;
    }

    get description() {
        return this._description;
    }
}
