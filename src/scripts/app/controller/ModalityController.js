class ModalityController {
    constructor() {
        this._modalities = {
            "classes": [
                {
                    "title": "Natação",
                    "type" : "aquatic",
                    "description": "A prática da natação é considerada um dos melhores exercícios físicos existentes trazendo ótimos benefícios para o organismo e benefícios a saúde e é a única actividade física indicada para menores de 3 anos."
                },
                {
                    "title": "Aeróbica",
                    "type" : "non-aquatic",
                    "description": "Os exercícios aeróbicos usam grandes grupos musculares, rítmica e continuamente, elevando os batimentos cardíacos e a respiração durante algum tempo. O exercício aeróbico é longo em duração e moderada em intensidade."
                },
                {
                    "title": "Dança",
                    "type" : "non-aquatic",
                    "description": "Dançar pode auxiliar no tratamento de doenças como diabetes, síndrome do pânico, transtorno bipolar, depressão e até alguns tipos de câncer. A dança pode ser considerada um remédio que melhora a saúde física e mental."
                },
                {
                    "title": "Hidroginástica",
                    "type" : "aquatic",
                    "description": "A prática da hidroginástica pode levar a menor impacto nas articulações, menor esforço nos movimentos e sensação de conforto causada pela água. A hidroginástica é indicada como atividade anaeróbica, pura e simples, e também indicada para aqueles que dela realmente precisam incluindo os portadores de problemas de peso, os mais diversos."
                    },
                {
                    "title": "Corrida",
                    "type": "non-aquatic",
                    "description": "É um dos exercícios mais recomendados pelos médicos, seja para saúde como para perda de peso, além de proporcionar bem-estar físico e mental. Essa atividade é completa e envolve praticamente todos os músculos do corpo, aumentando o ganho de massa muscular e queima de calorias"
                },
                {
                    "title": "Pilates",
                    "type": "non-aquatic",
                    "description": "O Pilates mistura um treino de força e flexibilidade que ajuda a melhorar a postura, alongar e tonificar os músculos sem exageros. Os exercícios focam na qualidade dos movimentos, ao invés da quantidade, deixando o praticante revigorado após a prática."
                }
            ]
        };

        this._modalityList = new ModalityList();

        this._modalityView = new ModalityView($('#modalities'));

        this._selectedFilters = new Array();

        this._filteredModalities = new Array();
    }

    loadModalities() {
        this._modalities['classes'].forEach(m => {
            let modality = new Modality(
                m.title,
                m.type,
                m.description
            );

            this._modalityList.add(modality);
        });

        this._modalityView.update(this._modalityList.modalities);
    }

    _addFilter(filter) {
        if (this._selectedFilters.indexOf(filter) === -1) {
            this._selectedFilters.push(filter);
        }
    }

    _removeFilter(filter) {
        if (this._selectedFilters.indexOf(filter) !== -1) {
            let filterIndex = this._selectedFilters.indexOf(filter);

            this._selectedFilters.splice(filterIndex, 1);
        }
    }

    _applyFilters() {
        this._filteredModalities = [];

        this._selectedFilters.forEach(filter => {
            this._modalityList.modalities.forEach(modality => {
                if (modality.type == filter) {
                    this._filteredModalities.push(modality);
                }
            });
        });
    }

    filterModalities(event) {
        let checkbox = event.target;
        let modalityType = checkbox.dataset.modalityType;

        if (checkbox.checked) {
            this._addFilter(modalityType)
        } else {
            this._removeFilter(modalityType);
        }

        this._selectedFilters.sort();

        this._applyFilters();

        if (! this._selectedFilters.length) {
            this._filteredModalities = this._modalityList.modalities;
        }

        this._modalityView.update(this._filteredModalities);
    }
}
