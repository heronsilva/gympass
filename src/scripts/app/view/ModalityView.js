class ModalityView {
    constructor(element) {
        this._element = element;
    }

    _createSlider() {
        let slider = tns({
            container: document.querySelector('.slider'),
            autoplay: false,
            loop: false,
            gutter: 15,
            controls: false,
            slideBy: 'page',
            responsive: {
                320: 1,
                480: 2,
                768: 3,
                1024: 4
            }
        });
    }

    _render(model) {
        return `
            <ul class="modality-list slider">
                ${ model.map(m => `
                    <li class="item">
                        <h4 class="title">
                            ${ m.title }
                        </h4>

                        <p class="paragraph">
                            ${ m.description }
                        </p>
                    </li>
                `).join('') }
            </ul>
        `;
    }

    update(model) {
        this._element.innerHTML = this._render(model);

        this._createSlider();
    }
}
