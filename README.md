# Projeto de avaliação da Gympass.

## Tecnologias utilizadas
* [rscss](http://rscss.io/)

    Utilizado como padrão de escrita de CSS.
    Foi escolhido por propor uma escrita menos verbosa em relação a outros padrões como, por exemplo, o [BEM](http://getbem.com/).

* SASS

    A implementação utilizada durante o desenvolvimento foi o [node-sass](https://github.com/sass/node-sass), ao invés da tradicional implementação ruby-sass.

* Gulp.js

    Utilizado como task runner padrão. O escolhi por ter maior afinidade e por me oferecer uma escrita mais simples.

* Bower e Npm, como gerenciadores de dependências

* Git, como controle de versionamento.

* ES2015 e orientação a objetos.

    Por utilizar estes recursos, se fez necessário também utilizar o *transpiler* [Babel](https://babeljs.io/), para oferecer compatibilidade para navegadores anteriores e possibilitar o uso do plugin *uglify*, no Gulp.

## Como rodar o projeto

1. Clone o repositório

    `git clone https://gitlab.com/heronsilva/gympass.git`

2. Instale as dependências

    `bower install && npm install`

3. Realize a "compilação" dos assets

    `gulp`

4. Abra o arquivo index.html localizado no diretório `dist`

    4.1 Se preferir, é possível subir um servidor. Basta executar o comando `gulp` com a flag `--server`, e acessar o endereço exibido em seu output.
