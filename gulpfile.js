let gulp = require('gulp');

let argv = require('yargs').argv;

let $ = require('gulp-load-plugins')({ pattern: '*' });

gulp.task('default', ['copy'], () => {
    $.sequence('css-sprites', ['sass', 'transpile', 'imagemin'], 'usemin')();

    if (argv.server) {
        $.connect.server({
            root: 'dist'
        });
    }
});

gulp.task('clean', () => {
    return gulp.src('dist').pipe($.clean());
});

gulp.task('copy', ['clean'], () => {
    return gulp.src('src/index.html', { base: 'src' })
               .pipe(gulp.dest('dist'));
});

gulp.task('imagemin', () => {
    return gulp.src('src/images/*.{jpg,png}')
               .pipe($.imagemin())
               .pipe(gulp.dest('dist/images'));
});

gulp.task('css-sprites', () => {
    let spritedata = gulp.src('src/images/icons/*.png').pipe($.spritesmith({
        imgName: 'sprite.png',
        imgPath: '../images/sprite.png',
        cssName: 'sprite.scss',
        cssFormat: 'scss'
    }));

    let imgStream = spritedata.img
                              .pipe($.vinylBuffer())
                              .pipe($.imagemin())
                              .pipe(gulp.dest('src/images/'));

    let cssStream = spritedata.css
                              .pipe(gulp.dest('src/stylesheets/mixins'));

    return $.mergeStream(imgStream, cssStream);
});

gulp.task('sass', ['css-sprites'], () => {
    return gulp.src('src/stylesheets/main.scss')
               .pipe($.sassGlob())
               .pipe($.sass().on('error', $.sass.logError))
               .pipe($.autoprefixer())
               .pipe(gulp.dest('src/css/'))
               .pipe($.browserSync.stream());
});

gulp.task('sass:watch', () => {
    gulp.watch('src/stylesheets/**/*', ['sass']);
});

gulp.task('transpile', () => {
    return gulp.src('src/scripts/**/*.js')
               .pipe($.babel({
                   presets: ['es2015']
               }))
               .pipe(gulp.dest('src/js'));
});

gulp.task('usemin', () => {
    return gulp.src('src/index.html')
               .pipe($.usemin({
                   js: [$.uglify],
                   css: [$.cssnano]
               }))
               .pipe(gulp.dest('dist'));
});

gulp.task('server', ['sass', 'transpile'], () => {
    $.browserSync.init({
        open: false,
        server: {
            baseDir: 'src',
            routes: {
                '/bower_components': 'bower_components'
            }
        }
    });

    gulp.watch('src/stylesheets/**/*', ['sass']);

    gulp.watch('src/scripts/**/*', ['transpile']);

    gulp.watch('src/index.html').on('change', $.browserSync.reload);
});
